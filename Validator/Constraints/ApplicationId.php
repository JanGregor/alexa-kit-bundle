<?php

namespace JanGregor\AlexaKitBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ApplicationId extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The id "{{ string }}" is not configured in your application';
}
