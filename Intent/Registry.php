<?php

namespace JanGregor\AlexaKitBundle\Intent;

use Doctrine\Common\Collections\ArrayCollection;

use JanGregor\AlexaKitBundle\Common\IntentInterface;
use JanGregor\AlexaKitBundle\Exception\RegistryException;
use JanGregor\AlexaKitBundle\Model\Request\AlexaRequest;
use JanGregor\AlexaKitBundle\Model\Response\AlexaResponse;

class Registry
{
    /**
     * @var ArrayCollection|IntentInterface[]
     */
    protected $intents;

    /**
     *
     */
    public function __construct()
    {
        $this->intents = new ArrayCollection();
    }

    /**
     * @return string[]
     */
    public function getRegisteredIntentNames()
    {
        return $this->intents->getKeys();
    }

    /**
     * @param IntentInterface $intent
     *
     * @throws RegistryException
     */
    public function registerIntent(IntentInterface $intent)
    {
        foreach ($intent->getIntentNames() as $intentName) {
            if ($this->intents->containsKey($intentName)) {
                throw RegistryException::createDuplicateNameException($intentName);
            }

            $this->intents->set($intentName, $intent);
        }
    }

    /**
     * @param AlexaRequest $request
     *
     * @return AlexaResponse
     *
     * @throws RegistryException
     */
    public function processRequest(AlexaRequest $request): AlexaResponse
    {
        /** @var IntentInterface|null $intent */
        $intentName = $request->getRequest()->getIntent()->getName();
        $intent     = $this->intents->get($intentName);

        if (!$intent) {
            throw RegistryException::createUnknownIntentException($this, $intentName);
        }

        $response = $intent->processRequest($request);

        return $response;
    }
}
