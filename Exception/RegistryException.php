<?php

namespace JanGregor\AlexaKitBundle\Exception;


use JanGregor\AlexaKitBundle\Intent\Registry;

class RegistryException extends \Exception
{
    /**
     * @param string $name
     *
     * @return RegistryException
     */
    public static function createDuplicateNameException(string $name)
    {
        return new self(sprintf('Intent with name "%s" is already registered.', $name));
    }

    /**
     * @param Registry $registry
     * @param string   $name
     *
     * @return RegistryException
     */
    public static function createUnknownIntentException(Registry $registry, string $name)
    {
        return new self(sprintf('No known intent with name "%s" is registered. Please choose from: %s',
            $name,
            implode(', ', $registry->getRegisteredIntentNames())
        ));
    }
}
