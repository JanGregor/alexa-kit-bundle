<?php

namespace JanGregor\AlexaKitBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use JanGregor\AlexaKitBundle\Form\Request\AlexaRequestType;

class BuildRequestCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('alexa-kit:build-request')
            ->setDescription('Building a request object from a (valid) json file')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to json file')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = new \SplFileObject($input->getArgument('path'));
        $output->writeln(sprintf('Beginning to read file at: %s', $file->getRealPath()));

        $json = file_get_contents($file->getRealPath());

        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $array      = $serializer->decode($json, 'json');

        $form = $this->getContainer()->get('form.factory')->create(AlexaRequestType::class);

        $form->submit($array);

        if (!$form->isValid()) {
            $output->writeln(sprintf('Encountered errors during form binding.'));
            return;
        }

        $model = $form->getData();

        $response = $this->getContainer()->get('jan_gregor.alexa_kit.intent.registry')->processRequest($model);

        $jsonContent = $serializer->serialize($response, 'json');


        dump($jsonContent);
    }
}
