<?php

namespace JanGregor\AlexaKitBundle\Common;

use JanGregor\AlexaKitBundle\Model\Request\AlexaRequest;
use JanGregor\AlexaKitBundle\Model\Response\AlexaResponse;

interface IntentInterface
{
    /**
     * @return string[]
     */
    public function getIntentNames(): array;

    /**
     * @param AlexaRequest $request
     *
     * @return AlexaResponse
     */
    public function processRequest(AlexaRequest $request): AlexaResponse;
}
