<?php

namespace JanGregor\AlexaKitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

use JanGregor\AlexaKitBundle\Model\Session;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sessionId', TextType::class)
            ->add('application', ApplicationType::class)
            ->add('attributes', CollectionType::class, [
                'allow_add'    => true,
                'allow_delete' => true,
            ])
            ->add('user', UserType::class)
            ->add('new', CheckboxType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'      => Session::class,
            'constraints'     => [new Valid()],
        ]);
    }
}
