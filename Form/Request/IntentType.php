<?php

namespace JanGregor\AlexaKitBundle\Form\Request;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use JanGregor\AlexaKitBundle\Model\Request\Intent;

class IntentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('confirmationStatus', TextType::class, [
                'mapped' => false,
            ])
            ->add('slots', CollectionType::class, [
                'entry_type' => SlotType::class,
                'allow_add' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'      => Intent::class,
        ]);
    }
}
