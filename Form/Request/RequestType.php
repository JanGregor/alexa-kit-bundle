<?php

namespace JanGregor\AlexaKitBundle\Form\Request;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use JanGregor\AlexaKitBundle\Model\Request\Request;

class RequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                  Request::TYPE_INTENT_REQUEST => Request::TYPE_INTENT_REQUEST,
                ],
            ])
            ->add('requestId', TextType::class)
            ->add('locale', LocaleType::class)
            ->add('timestamp', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('intent', IntentType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'      => Request::class,
        ]);
    }
}
