<?php

namespace JanGregor\AlexaKitBundle\Form\Request;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

use JanGregor\AlexaKitBundle\Form\ContextType;
use JanGregor\AlexaKitBundle\Form\SessionType;
use JanGregor\AlexaKitBundle\Model\Request\AlexaRequest;

class AlexaRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('session', SessionType::class)
            ->add('request', RequestType::class)
            ->add('context', ContextType::class, [
                'mapped' => false,
            ])
            ->add('version', ChoiceType::class, [
                'choices' => [
                  '1.0' => '1.0'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class'      => AlexaRequest::class,
            'constraints'     => [new Valid()],
        ]);
    }
}
