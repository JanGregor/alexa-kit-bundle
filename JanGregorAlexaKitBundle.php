<?php

namespace JanGregor\AlexaKitBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use JanGregor\AlexaKitBundle\DependencyInjection\CompilerPass\IntentRegistryCompilerPass;

class JanGregorAlexaKitBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new IntentRegistryCompilerPass());
    }
}
