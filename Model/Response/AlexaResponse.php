<?php

namespace JanGregor\AlexaKitBundle\Model\Response;

class AlexaResponse
{
    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var Response;
     */
    protected $response;

    /**
     * @var string
     */
    protected $version =  '1.0';

    /**
     *
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }
}
