<?php

namespace JanGregor\AlexaKitBundle\Model\Response;

class OutputSpeech
{
    const TYPE_PLAIN_TEXT = 'PlainText';
    const TYPE_SSML       = 'SSML';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $ssml;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->type = self::TYPE_PLAIN_TEXT;

        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getSsml()
    {
        return $this->ssml;
    }

    /**
     * @param string $ssml
     */
    public function setSsml(string $ssml)
    {
        $this->type = self::TYPE_SSML;

        $this->ssml = $ssml;
    }
}
