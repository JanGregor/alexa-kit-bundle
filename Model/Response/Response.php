<?php

namespace JanGregor\AlexaKitBundle\Model\Response;

class Response
{
    /**
     * @var boolean
     */
    protected $shouldEndSession = true;

    /**
     * @var OutputSpeech
     */
    protected $outputSpeech;

    /**
     * @return bool
     */
    public function isShouldEndSession(): bool
    {
        return $this->shouldEndSession;
    }

    /**
     * @param bool $shouldEndSession
     */
    public function setShouldEndSession(bool $shouldEndSession)
    {
        $this->shouldEndSession = $shouldEndSession;
    }

    /**
     * @return OutputSpeech
     */
    public function getOutputSpeech(): OutputSpeech
    {
        if (!$this->outputSpeech) {
            $this->outputSpeech = new OutputSpeech();
        }

        return $this->outputSpeech;
    }

    /**
     * @param OutputSpeech|null $outputSpeech
     */
    public function setOutputSpeech(OutputSpeech $outputSpeech = null)
    {
        $this->outputSpeech = $outputSpeech;
    }
}
