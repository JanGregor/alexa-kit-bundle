<?php

namespace JanGregor\AlexaKitBundle\Model;

use JanGregor\AlexaKitBundle\Validator\Constraints as AlexaKitAssert;

class Application
{
    /**
     * @AlexaKitAssert\ApplicationId()
     *
     * @var string
     */
    protected $applicationId;

    /**
     * @return string
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * @param string $applicationId
     */
    public function setApplicationId($applicationId)
    {
        $this->applicationId = $applicationId;
    }
}
