<?php

namespace JanGregor\AlexaKitBundle\Model\Request;

class Intent
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var Slot[]
     */
    protected $slots;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Slot[]
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * @param Slot[] $slots
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    }
}
