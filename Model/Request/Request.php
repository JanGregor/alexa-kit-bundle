<?php

namespace JanGregor\AlexaKitBundle\Model\Request;

class Request
{
    const TYPE_INTENT_REQUEST = 'IntentRequest';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $requestId;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @var Intent
     */
    protected $intent;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return Intent
     */
    public function getIntent()
    {
        return $this->intent;
    }

    /**
     * @param Intent $intent
     */
    public function setIntent($intent)
    {
        $this->intent = $intent;
    }
}
