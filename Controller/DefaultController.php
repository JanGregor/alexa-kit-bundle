<?php

namespace JanGregor\AlexaKitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use JanGregor\AlexaKitBundle\Form\Request\AlexaRequestType;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->get('logger')->notice('Incoming alexa request.', [
            'request' => $request->request->all(),
            'body'    => $request->getContent(),
        ]);

        try {
            $json = $request->getContent();

            $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
            $array      = $serializer->decode($json, 'json');

            $form = $this->get('form.factory')->create(AlexaRequestType::class);

            unset($array['context']);

            $form->submit($array);

            if (!$form->isValid()) {

                throw new \Exception('Form invalid !');
            }

            $model = $form->getData();

            $response = $this->get('jan_gregor.alexa_kit.intent.registry')->processRequest($model);

            $jsonContent = $serializer->serialize($response, 'json');

            $this->get('logger')->notice('Returning alexa response.', [
                'body'    => $jsonContent,
            ]);

            $httpResponse = new Response($jsonContent, 200, [
                'Content-Type' => 'application/json;charset=UTF-8',
            ]);

            return $httpResponse;
        } catch (\Exception $e) {

            $this->get('logger')->notice('Exception encountered.', [
                'message'    => $e->getMessage(),
            ]);


            return new Response('', 500);
        }
    }
}
