<?php

namespace JanGregor\AlexaKitBundle\DependencyInjection\CompilerPass;

class IntentRegistryCompilerPass extends AbstractCompilerPass
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct(
            'jan_gregor.alexa_kit.intent.registry',
            'alexa_kit.intent',
            'registerIntent'
        );
    }
}
