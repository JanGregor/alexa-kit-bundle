<?php

namespace JanGregor\AlexaKitBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class AbstractCompilerPass
 *
 * This class can be extended from in order to easily collect other services
 */
abstract class AbstractCompilerPass implements CompilerPassInterface
{
    /**
     * The service responsible form collection other tagged services
     *
     * @var string
     */
    protected $registryService;

    /**
     * The tag to lookout for
     *
     * @var string
     */
    protected $tag;

    /**
     * The method call on the registry service to add a found tagged service
     *
     * @param string
     */
    protected $methodCall;

    /**
     * @param string $registryService
     * @param string $tag
     * @param string $methodCall
     */
    public function __construct(string $registryService, string $tag, string $methodCall)
    {
        $this->registryService = $registryService;
        $this->tag             = $tag;
        $this->methodCall      = $methodCall;
    }

    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition($this->registryService)) {
            return;
        }

        $definition     = $container->getDefinition($this->registryService);
        $taggedServices = $container->findTaggedServiceIds($this->tag);

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall($this->methodCall , array($container->getDefinition($id), $attributes));
            }
        }
    }
} 
